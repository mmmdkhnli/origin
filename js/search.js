$('.search-bar').click(function(){
    $('.search-content').fadeIn(200);
    $('.search-content').css('display','flex');
    $('.list-desktop-cover').fadeIn(200);
});

$('.close-item').click(function(){
    $('.search-content').fadeOut(200);
    $('.list-desktop-cover').fadeOut(200);
});