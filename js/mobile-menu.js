$('.mobile-menu-button').click(function(){
    $('.mobile-menu').css('margin-left','0px').css('transition','0.2s all');
    $('.list-desktop-cover').fadeIn(200);
});

$('.close-menu').click(function(){
    $('.mobile-menu').css('margin-left','200vw').css('transition','0.2s all');
    $('.list-desktop-cover').fadeOut(200);
});