//owl-carousel 1

$(document).ready(function () {
    $("#owl-example1").owlCarousel({
        margin: 5,
        loop: true,
        callbacks: true,
        items: 1,
        slideSpeed: 500,
        autoplay: true,
        nav: false,
        autoPlaySpeed: 500,
        smartSpeed: 500,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
                loop: true
            }
        }
    });
});


//owl-carousel 2

$(document).ready(function () {
    $("#owl-example2").owlCarousel({
        margin: 5,
        loop: true,
        callbacks: true,
        items: 1,
        slideSpeed: 500,
        autoplay: true,
        nav: false,
        autoPlaySpeed: 500,
        smartSpeed: 500,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 3,
                loop: true
            }
        }
    });
});



$('.li1').hover(function(){
    $('.open1').fadeIn(10);
    $(this).css('background-color','#1292C0');
    $('.li1 a').css('color','white');
    $('.list-desktop-cover').fadeIn(0);
});

$('.li1').mouseleave(function(){
    $('.open1').fadeOut(10);
    $(this).css('background-color','white');
    $('.li1 a').css('color','rgb(102, 102, 102)');
    $('.list-desktop-cover').fadeOut(0);
});

$('.li2').mouseleave(function(){
    $(this).css('background-color','white');
    $('.li2 a').css('color','rgb(102, 102, 102)');
});

$('.li2').hover(function(){
    $(this).css('background-color','#1292C0');
    $('.li2 a').css('color','white');
});

$('.li3').hover(function(){
    $(this).css('background-color','#1292C0');
    $('.li3 a').css('color','white');
});

$('.li3').mouseleave(function(){
    $(this).css('background-color','white');
    $('.li3 a').css('color','rgb(102, 102, 102)');
});

$('.li4').hover(function(){
    $(this).css('background-color','#1292C0');
    $('.li4 a').css('color','white');
});

$('.li4').mouseleave(function(){
    $(this).css('background-color','white');
    $('.li4 a').css('color','rgb(102, 102, 102)');
});

$('.li5').hover(function(){
    $('.open4').fadeIn(10);
    $(this).css('background-color','#1292C0');
    $('.li5 a').css('color','white');
    $('.list-desktop-cover').fadeIn(0);
});

$('.li5').mouseleave(function(){
    $('.open4').fadeOut(10);
    $(this).css('background-color','white');
    $('.li5 a').css('color','rgb(102, 102, 102)');
    $('.list-desktop-cover').fadeOut(0);
});

$('.li6').hover(function(){
    $(this).css('background-color','#1292C0');
    $('.li6 a').css('color','white');
});

$('.li6').mouseleave(function(){
    $(this).css('background-color','white');
    $('.li6 a').css('color','rgb(102, 102, 102)');
});


$('.m_l1').click(function(){
    $('.m_l1>.m_open').slideToggle();
});

$('.m_l2').click(function(){
    $('.m_l2>.m_open').slideToggle();
});

$('.m_l3').click(function(){
    $('.m_l3>.m_open').slideToggle();
});

$('.m_l4').click(function(){
    $('.m_l4>.m_open').slideToggle();
});












